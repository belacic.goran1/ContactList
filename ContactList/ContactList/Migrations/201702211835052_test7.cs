namespace ContactList.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test7 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Emails", "Contact_Id", "dbo.Contacts");
            DropForeignKey("dbo.PhoneNumbers", "Contact_Id", "dbo.Contacts");
            DropIndex("dbo.Emails", new[] { "Contact_Id" });
            DropIndex("dbo.PhoneNumbers", new[] { "Contact_Id" });
            AddColumn("dbo.Emails", "Contact_Id1", c => c.Int());
            AddColumn("dbo.PhoneNumbers", "Contact_Id1", c => c.Int());
            AlterColumn("dbo.Emails", "Contact_Id", c => c.Int(nullable: false));
            AlterColumn("dbo.PhoneNumbers", "Contact_Id", c => c.Int(nullable: false));
            CreateIndex("dbo.Emails", "Contact_Id1");
            CreateIndex("dbo.PhoneNumbers", "Contact_Id1");
            AddForeignKey("dbo.Emails", "Contact_Id1", "dbo.Contacts", "Id");
            AddForeignKey("dbo.PhoneNumbers", "Contact_Id1", "dbo.Contacts", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PhoneNumbers", "Contact_Id1", "dbo.Contacts");
            DropForeignKey("dbo.Emails", "Contact_Id1", "dbo.Contacts");
            DropIndex("dbo.PhoneNumbers", new[] { "Contact_Id1" });
            DropIndex("dbo.Emails", new[] { "Contact_Id1" });
            AlterColumn("dbo.PhoneNumbers", "Contact_Id", c => c.Int());
            AlterColumn("dbo.Emails", "Contact_Id", c => c.Int());
            DropColumn("dbo.PhoneNumbers", "Contact_Id1");
            DropColumn("dbo.Emails", "Contact_Id1");
            CreateIndex("dbo.PhoneNumbers", "Contact_Id");
            CreateIndex("dbo.Emails", "Contact_Id");
            AddForeignKey("dbo.PhoneNumbers", "Contact_Id", "dbo.Contacts", "Id");
            AddForeignKey("dbo.Emails", "Contact_Id", "dbo.Contacts", "Id");
        }
    }
}
