namespace ContactList.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Emails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmailAddress = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PhoneNumbers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Number = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EmailContacts",
                c => new
                    {
                        Email_Id = c.Int(nullable: false),
                        Contact_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Email_Id, t.Contact_Id })
                .ForeignKey("dbo.Emails", t => t.Email_Id, cascadeDelete: true)
                .ForeignKey("dbo.Contacts", t => t.Contact_Id, cascadeDelete: true)
                .Index(t => t.Email_Id)
                .Index(t => t.Contact_Id);
            
            CreateTable(
                "dbo.PhoneNumberContacts",
                c => new
                    {
                        PhoneNumber_Id = c.Int(nullable: false),
                        Contact_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PhoneNumber_Id, t.Contact_Id })
                .ForeignKey("dbo.PhoneNumbers", t => t.PhoneNumber_Id, cascadeDelete: true)
                .ForeignKey("dbo.Contacts", t => t.Contact_Id, cascadeDelete: true)
                .Index(t => t.PhoneNumber_Id)
                .Index(t => t.Contact_Id);
            
            AlterColumn("dbo.Contacts", "FirstName", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.Contacts", "LastName", c => c.String(nullable: false, maxLength: 20));
            DropColumn("dbo.Contacts", "Email");
            DropColumn("dbo.Contacts", "PhoneNumber");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Contacts", "PhoneNumber", c => c.String());
            AddColumn("dbo.Contacts", "Email", c => c.String());
            DropForeignKey("dbo.PhoneNumberContacts", "Contact_Id", "dbo.Contacts");
            DropForeignKey("dbo.PhoneNumberContacts", "PhoneNumber_Id", "dbo.PhoneNumbers");
            DropForeignKey("dbo.EmailContacts", "Contact_Id", "dbo.Contacts");
            DropForeignKey("dbo.EmailContacts", "Email_Id", "dbo.Emails");
            DropIndex("dbo.PhoneNumberContacts", new[] { "Contact_Id" });
            DropIndex("dbo.PhoneNumberContacts", new[] { "PhoneNumber_Id" });
            DropIndex("dbo.EmailContacts", new[] { "Contact_Id" });
            DropIndex("dbo.EmailContacts", new[] { "Email_Id" });
            AlterColumn("dbo.Contacts", "LastName", c => c.String());
            AlterColumn("dbo.Contacts", "FirstName", c => c.String());
            DropTable("dbo.PhoneNumberContacts");
            DropTable("dbo.EmailContacts");
            DropTable("dbo.PhoneNumbers");
            DropTable("dbo.Emails");
        }
    }
}
