namespace ContactList.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test3 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.EmailContacts", "Email_Id", "dbo.Emails");
            DropForeignKey("dbo.EmailContacts", "Contact_Id", "dbo.Contacts");
            DropForeignKey("dbo.PhoneNumberContacts", "PhoneNumber_Id", "dbo.PhoneNumbers");
            DropForeignKey("dbo.PhoneNumberContacts", "Contact_Id", "dbo.Contacts");
            DropIndex("dbo.EmailContacts", new[] { "Email_Id" });
            DropIndex("dbo.EmailContacts", new[] { "Contact_Id" });
            DropIndex("dbo.PhoneNumberContacts", new[] { "PhoneNumber_Id" });
            DropIndex("dbo.PhoneNumberContacts", new[] { "Contact_Id" });
            AddColumn("dbo.Emails", "Contact_Id", c => c.Int());
            AddColumn("dbo.PhoneNumbers", "Contact_Id", c => c.Int());
            CreateIndex("dbo.Emails", "Contact_Id");
            CreateIndex("dbo.PhoneNumbers", "Contact_Id");
            AddForeignKey("dbo.Emails", "Contact_Id", "dbo.Contacts", "Id");
            AddForeignKey("dbo.PhoneNumbers", "Contact_Id", "dbo.Contacts", "Id");
            DropTable("dbo.EmailContacts");
            DropTable("dbo.PhoneNumberContacts");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.PhoneNumberContacts",
                c => new
                    {
                        PhoneNumber_Id = c.Int(nullable: false),
                        Contact_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PhoneNumber_Id, t.Contact_Id });
            
            CreateTable(
                "dbo.EmailContacts",
                c => new
                    {
                        Email_Id = c.Int(nullable: false),
                        Contact_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Email_Id, t.Contact_Id });
            
            DropForeignKey("dbo.PhoneNumbers", "Contact_Id", "dbo.Contacts");
            DropForeignKey("dbo.Emails", "Contact_Id", "dbo.Contacts");
            DropIndex("dbo.PhoneNumbers", new[] { "Contact_Id" });
            DropIndex("dbo.Emails", new[] { "Contact_Id" });
            DropColumn("dbo.PhoneNumbers", "Contact_Id");
            DropColumn("dbo.Emails", "Contact_Id");
            CreateIndex("dbo.PhoneNumberContacts", "Contact_Id");
            CreateIndex("dbo.PhoneNumberContacts", "PhoneNumber_Id");
            CreateIndex("dbo.EmailContacts", "Contact_Id");
            CreateIndex("dbo.EmailContacts", "Email_Id");
            AddForeignKey("dbo.PhoneNumberContacts", "Contact_Id", "dbo.Contacts", "Id", cascadeDelete: true);
            AddForeignKey("dbo.PhoneNumberContacts", "PhoneNumber_Id", "dbo.PhoneNumbers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.EmailContacts", "Contact_Id", "dbo.Contacts", "Id", cascadeDelete: true);
            AddForeignKey("dbo.EmailContacts", "Email_Id", "dbo.Emails", "Id", cascadeDelete: true);
        }
    }
}
