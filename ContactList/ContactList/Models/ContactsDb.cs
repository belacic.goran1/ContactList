﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;





namespace ContactList.Models
{
    public class ContactsDb : DbContext
    {
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Email> Emails { get; set; }
        public DbSet<PhoneNumber> PhoneNumbers { get; set; }

       
    }
}