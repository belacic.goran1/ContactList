﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace ContactList.Models
{
    public class Contact
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "First name is required")]
        [StringLength(20, MinimumLength = 3, ErrorMessage = "Name should not be longer than 20 characters.")]
        [Display(Name = "First name ")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Last name is required")]
        [StringLength(20, MinimumLength = 3, ErrorMessage = "Last name should not be longer than 20 characters.")]
        [Display(Name = "Last name ")]
        public string LastName { get; set; }
        [Display(Name = "City ")]
        public string City { get; set; }
        [Display(Name = "Address ")]
        public string Address { get; set; }
        
        public bool IsBookmarked { get; set; }

        public bool isDeleted { get; set; }        
        
        public virtual string TagsListing { get; set; }
        public virtual List<Tag> Tags { get; set; }
        public virtual List<Email> Emails { get; set; }
        public virtual List<PhoneNumber> PhoneNumbers { get; set; }

    }
}