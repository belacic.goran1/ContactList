﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(vrati.Startup))]
namespace vrati
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
